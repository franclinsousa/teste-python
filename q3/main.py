from app.bootloader.application import App
from app.resource import register_blueprints

app = App(__name__)
register_blueprints(app)

if __name__ == "__main__":
    app.run()
