from typing import List

from flask import Blueprint, request, json, jsonify, make_response, abort
from flask.helpers import BadRequest
from flask.app import Response

from app.domain.model.user import User
from app.infrastructure.json.json_file_db import UserJsonFileDBRepository
from app.service import UserService

bp_user = Blueprint("user", "user_api", url_prefix="/user")

repos = UserJsonFileDBRepository()
svc = UserService(repos)


@bp_user.route("", methods=["POST", "PUT"])
def post_put():
    data = dict(request.get_json())
    user: User = User(nome=data.get("nome"), identifier=data.get("id"))
    if user is None or user.nome is None:
        return Response(
            json.dumps(
                {
                    "mensagem": "Campo 'nome' é necessário."
                }
            ),
            status=400,
            mimetype="application/json"
        )
    svc.save(user)
    return Response(status=200)


@bp_user.route("/<id>", methods=["DELETE"])
def delete(id):
    svc.delete(id)
    return Response(status=200)


@bp_user.route("", methods=["GET"])
def get():
    users: List[User] = svc.get_all()
    return Response(
        json.dumps(users),
        status=200,
        mimetype="application/json"
    )
