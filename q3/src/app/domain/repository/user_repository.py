import abc
from typing import List, Optional

from app.domain.model import User


class UserRepository(metaclass=abc.ABCMeta):

    @classmethod
    def __subclasshook__(cls, subclass):
        return (
                hasattr(subclass, "save") and callable(subclass.save) and
                hasattr(subclass, "delete") and callable(subclass.delete) and
                hasattr(subclass, "find_by_all") and callable(subclass.find_by_all) or
                NotImplemented
        )

    @abc.abstractmethod
    def save(self, user: User) -> Optional[User]:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, identifier: int) -> Optional[bool]:
        raise NotImplementedError

    @abc.abstractmethod
    def find_by_all(self) -> Optional[List[User]]:
        raise NotImplementedError
