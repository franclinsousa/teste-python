
class User(object):
    def __init__(self, nome: str, identifier: int = None):
        self.identifier = identifier
        self.nome = nome

    def to_serializable(self):
        return {
            "id": self.identifier,
            "nome": self.nome
        }
