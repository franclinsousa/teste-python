import os
from typing import Optional, List

from flask import json, jsonify, current_app, Flask

from app.domain.model import User
from app.domain.repository import UserRepository


class UserJsonFileDBRepository(UserRepository):

    def __init__(self):
        try:
            f = open(Flask.auto_find_instance_path(current_app) + "/db.json", "r")
            f.close()
        except:
            pass

    def save(self, user: User) -> Optional[User]:
        users_db = open(Flask.auto_find_instance_path(current_app) + "/db.json", "r")
        users_json: list = list(json.loads(users_db.read()))
        users_db.close()
        users_json.append(user.to_serializable())
        users_db = open(Flask.auto_find_instance_path(current_app) + "/db.json", "w")
        users_db.write(json.dumps(users_json))
        users_db.close()
        return user

    def delete(self, identifier: int) -> Optional[bool]:
        pass

    def find_by_all(self) -> Optional[List[User]]:
        pass
